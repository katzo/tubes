// This file is part of CPAchecker,
// a tool for configurable software verification:
// https://cpachecker.sosy-lab.org
//
// SPDX-FileCopyrightText: 2024 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.cpa.tube;

import com.google.common.collect.ImmutableSet;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.sosy_lab.cpachecker.cfa.model.CFAEdge;
import org.sosy_lab.cpachecker.cfa.model.CFANode;
import org.sosy_lab.cpachecker.core.AnalysisDirection;
import org.sosy_lab.cpachecker.core.algorithm.distributed_summaries.decomposition.BlockNode;
import org.sosy_lab.cpachecker.core.interfaces.AbstractQueryableState;
import org.sosy_lab.cpachecker.core.interfaces.Partitionable;
import org.sosy_lab.cpachecker.core.interfaces.Targetable;
import org.sosy_lab.cpachecker.cpa.block.BlockEntryReachedTargetInformation;
import org.sosy_lab.cpachecker.cpa.tube.TubeState;



public class TubeState implements AbstractQueryableState, Partitionable, Serializable {

  //private static final long serialVersionUID = 3805801L;

  private Set<CFAEdge> nonDetCalls;
  private Set<CFAEdge> assertCalls;

  public TubeState(
      Set<CFAEdge> pNonDetCalls,
      Set<CFAEdge> pAssertCalls

  ) {
    this.nonDetCalls = pNonDetCalls;
    this.assertCalls = pAssertCalls;
  }

  public TubeState() {
    Set<CFAEdge> asserts = getAsserts();
    Set<CFAEdge> nondetes = getNonDets();
  }


  public Set<CFAEdge> getNonDets() {
    return nonDetCalls;
  }


  public Set<CFAEdge> getAsserts() {
    return assertCalls;
  }

  @Override
  public String getCPAName() {
    return TubeCPA.class.getSimpleName();
  }

  @Override
  public @Nullable Object getPartitionKey() {
    return this;
  }

  @Override
  public String toString() {
    return "TubeState{" +
        "nonDetCalls=" + nonDetCalls +
        ", assertCalls=" + assertCalls +
        '}';
  }
}
