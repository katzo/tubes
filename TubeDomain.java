// This file is part of CPAchecker,
// a tool for configurable software verification:
// https://cpachecker.sosy-lab.org
//
// SPDX-FileCopyrightText: 2024 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.cpa.tube;

import com.google.common.collect.ImmutableSet;
import java.util.Collection;
import java.util.Set;
import org.eclipse.cdt.core.index.IPDOMASTProcessor.Abstract;
import org.sosy_lab.cpachecker.cfa.model.CFAEdge;
import org.sosy_lab.cpachecker.core.interfaces.AbstractDomain;
import org.sosy_lab.cpachecker.core.interfaces.AbstractState;
import org.sosy_lab.cpachecker.exceptions.CPAException;

public class TubeDomain implements AbstractDomain {
  @Override
  public AbstractState join(AbstractState state1, AbstractState state2)
      throws CPAException, InterruptedException {
    //TODO: convert to TubeState and then to sets (the non det and assert) and concatinate and eliminate duplicates (join). return a new TubeState of the joined


    TubeState e1 = (TubeState) state1;
    TubeState e2 = (TubeState) state2;

    Set<CFAEdge> nonDets1 = e1.getNonDets();
    Set<CFAEdge> nonDets2 = e2.getNonDets();

    nonDets1.addAll(nonDets2);

    Set<CFAEdge> nonAsserts1 = e1.getAsserts();
    Set<CFAEdge> nonAsserts2 = e2.getAsserts();

    nonAsserts1.addAll(nonAsserts1);

    TubeState tubeState = new TubeState(nonDets1,nonAsserts2);
    return tubeState;
  }

  @Override
  public boolean isLessOrEqual(AbstractState state1, AbstractState state2)
      throws CPAException, InterruptedException {
    //TODO: convert he states to sets and check if the 2 lists are equal - return True/False

    boolean answer = false;
    TubeState e1 = (TubeState) state1;
    TubeState e2 = (TubeState) state2;

    Set<CFAEdge> nonDets1 = e1.getNonDets();
    Set<CFAEdge> nonDets2 = e2.getNonDets();
    
    Set<CFAEdge> nonAsserts1 = e1.getAsserts();
    Set<CFAEdge> nonAsserts2 = e2.getAsserts();
    
    int sizeNonDets1 = nonDets1.size();
    int sizeNonDets2 = nonDets2.size();
    
    int sizeAsserts1 = nonAsserts1.size();
    int sizeAsserts2 = nonAsserts2.size();
    
    if (nonDets1.equals(nonDets2) || sizeNonDets1 <= sizeNonDets2){
      answer = true;
    }

    else if(nonAsserts1.equals(nonAsserts2) || sizeAsserts1 <= sizeAsserts2){
      answer = true;
    }
    
    return answer;
  }
}
