// This file is part of CPAchecker,
// a tool for configurable software verification:
// https://cpachecker.sosy-lab.org
//
// SPDX-FileCopyrightText: 2024 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.cpa.tube;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.sosy_lab.cpachecker.cfa.model.CFAEdge;
import org.sosy_lab.cpachecker.cfa.model.CFANode;
import org.sosy_lab.cpachecker.core.AnalysisDirection;
import org.sosy_lab.cpachecker.core.algorithm.distributed_summaries.decomposition.BlockNode;
import org.sosy_lab.cpachecker.core.defaults.SingleEdgeTransferRelation;
import org.sosy_lab.cpachecker.core.interfaces.AbstractState;
import org.sosy_lab.cpachecker.core.interfaces.Precision;
import org.sosy_lab.cpachecker.core.interfaces.TransferRelation;
import org.sosy_lab.cpachecker.cpa.tube.TubeState;

import org.sosy_lab.cpachecker.exceptions.CPATransferException;
import org.sosy_lab.cpachecker.util.CFAUtils;

public class TubeTransferRelation extends SingleEdgeTransferRelation {


  @Override
  public Collection<TubeState> getAbstractSuccessorsForEdge(
      AbstractState element, Precision prec, CFAEdge cfaEdge) {
    TubeState tubeState = (TubeState) element;
    // TODO: tube state -> edge -> new tube state
    Set<CFAEdge> newNonDets = new HashSet<>(tubeState.getNonDets());
    Set<CFAEdge> newAsserts = new HashSet<>(tubeState.getAsserts());
    if (cfaEdge.getCode().contains("nondet")){ //will it work? or should i use CodeOf?

      newNonDets.add(cfaEdge);



      // do i need to print somewhere the final list of all the locations of nondets/asserts?

      //return set of a single elemnet which is the succesor;   // should i return just the new state? i saved the non dets and asserts in a different strucuture?
    }
    if (cfaEdge.getCode().contains("assert")){

      newAsserts.add(cfaEdge);

      //return oldStates;   // should i return just the new state? i saved the non dets and asserts in a different strucuture?
    }
    return ImmutableSet.of(new TubeState(newNonDets,newAsserts));
  }

}
